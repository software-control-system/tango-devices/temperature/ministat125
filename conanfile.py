from conan import ConanFile

class Ministat125Recipe(ConanFile):
    name = "ministat125"
    executable = "ds_Ministat125"
    version = "1.2.9"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Alain Buteau"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/temperature/ministat125.git"
    description = "Ministat125 device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
