// MinistatCommunicationTask.cpp: implementation of the MinistatCommunicationTask class.
//
//////////////////////////////////////////////////////////////////////

#include "MinistatCommunicationTask.h"
#include <iomanip>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

namespace Ministat125_ns
{
  MinistatCommunicationTask::MinistatCommunicationTask(Tango::DeviceImpl* host_device,
    Tango::DeviceProxyHelper * dev_proxy)
    : yat::Task(),
    Tango::LogAdapter(host_device),
    serial_proxy_(dev_proxy),
    command_counter_(EXTERNAL_TEMP_REQUEST)
  {

  }

  MinistatCommunicationTask::~MinistatCommunicationTask()
  {

  }

  ////////////////////////////////////////////////////////////////////////////////
  //
  //      handle_message()
  //      
  //      called by yat, the message to process is the top message of the message queue 
  //
  ////////////////////////////////////////////////////////////////////////////////
  void MinistatCommunicationTask::handle_message (yat::Message& _msg)
    throw (yat::Exception)
  {        
    switch(_msg.type())
    {
      //- TASK_INIT ----------------------
    case yat::TASK_INIT:
      {
        DEBUG_STREAM<<" TASK_INIT entering ...  "<<endl;

        ministat_data_.temperature_ext=0;
        ministat_data_.temperature_int=0;
        ministat_data_.temperature_preset=0;
        ministat_data_.externProbeUsed=false;
        ministat_data_.error_occured=false;
        ministat_data_.regulation_enabled=false;
        ministat_data_.remote_mode=false;
        ministat_data_.has_error = false;
        ministat_data_.has_write_error = false;
        ministat_data_.error_origin = "";
        ministat_data_.error_desc = "";

        command_counter_= EXTERNAL_TEMP_REQUEST;
     }
      break;

      //- TASK_EXIT ----------------------
    case yat::TASK_EXIT:
      {
        DEBUG_STREAM<<" TASK_EXIT entering ...  "<<endl;
      }
      break;

      //- TASK_PERIODIC ------------------
    case yat::TASK_PERIODIC:
      {
        DEBUG_STREAM<<" TASK_PERIODIC entering ...  "<<endl;

        switch(command_counter_)
        {
        case EXTERNAL_TEMP_REQUEST:
          {
            yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);
            read_external_temperature();
          }
          break;

        case PRESET_TEMP_REQUEST:
          {
            yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);
            read_preset_temperature();
          }
          break;

        case INTERNAL_TEMP_REQUEST:
          {
            yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);
            read_internal_temperature();
          }
          break;

        case PROBE_USED_REQUEST:
          {
            yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);
            read_probe_used();
          }
          break;

          /*    case ERROR_REQUEST:
          {
          yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);
          read_error();
          break;
          */

          //
          //
          // this part has been removed from the code because the "KM?" command 
          //(regulation state request) crash the serial line.
          //

          /*    case REGULATION_MODE_REQUEST:
          {
          yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);
          read_regulation_mode();
           }
          break;*/

        default:
          ERROR_STREAM<<" Unknown command request !  "<<endl;
        }

        command_counter_++;

        if(command_counter_ > (NUMBER_OF_READ_COMMAND_USED - 1) )
        {
          command_counter_ = EXTERNAL_TEMP_REQUEST;
        }
      }
      break;

      //- WRITE_PRESET_TEMP -------------------
    case WRITE_PRESET_TEMP_MSG:
      {
       yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);
       DEBUG_STREAM<<" WRITE_PRESET_TEMP_MSG entering ...  "<<endl;
        double * t_preset;
        try{
          _msg.detach_data (t_preset);
        }
        catch (yat::Exception& ex)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::WRITE_PRESET_TEMP_MSG";
          ministat_data_.error_desc = "failed to WRITE_PRESET_TEMP_MSG, yat exception caught<"
                         +  std::string (ex.errors[0].desc) + "\n";
          return;
        }
        write_preset_temperature (*t_preset);
      }
      break;

      //- WRITE_REMOTE_MODE -------------------
    case IS_REMOTE_MSG:
      {
        DEBUG_STREAM<<" IS_REMOTE_MSG entering ...  "<<endl;
        yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);
        try{
          write_read_on_serialline ("remote",false);
          ministat_data_.has_write_error = false;
        }
        catch (Tango::DevFailed& df)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::IS_REMOTE_MSG";
          ministat_data_.error_desc = "failed to to IS_REMOTE_MSG, yat exception caught<"
                         +  std::string (df.errors[0].desc) + "\n";
          return;
        }
        catch (...)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::IS_REMOTE_MSG";
          ministat_data_.error_desc = "failed to writeIS_REMOTE_MSG, (...) caught\n";
          return;
        }

      }
      break;

    case IS_LOCAL_MSG:
      {
        DEBUG_STREAM<<" IS_LOCAL_MSG entering ...  "<<endl;
        yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);

        try{
          write_read_on_serialline("local",false);
          ministat_data_.has_write_error = false;
        }
        catch (Tango::DevFailed& df)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::IS_LOCAL_MSG";
          ministat_data_.error_desc = "failed to to IS_LOCAL_MSG, yat exception caught<"
                         +  std::string (df.errors[0].desc) + "\n";
          return;
        }
        catch (...)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::IS_LOCAL_MSG";
          ministat_data_.error_desc = "failed to IS_LOCAL_MSG, (...) caught\n";
          return;
        }
      }
      break;

      //- WRITE_EXT_PROB_USED -------------------             
    case IS_EXT_PROBE_USED_MSG:
      {
        DEBUG_STREAM<<" IS_EXT_PROBE_USED_MSG entering ...  "<<endl;
        yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);

        try{
          write_read_on_serialline("EXTERN!",false);
          ministat_data_.has_write_error = false;
        }
        catch (Tango::DevFailed& df)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::IS_EXT_PROBE_USED_MSG";
          ministat_data_.error_desc = "failed to to IS_EXT_PROBE_USED_MSG, yat exception caught<"
                         +  std::string (df.errors[0].desc) + "\n";
          return;
        }
        catch (...)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::IS_EXT_PROBE_USED_MSG";
          ministat_data_.error_desc = "failed to IS_EXT_PROBE_USED_MSG, (...) caught\n";
          return;
        }
      }
      break;

      //- WRITE_INT_PROB_USED -------------------             
    case IS_INT_PROBE_USED_MSG:
      {
        DEBUG_STREAM<<" IS_INT_PROBE_USED_MSG entering ...  "<<endl;
        yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);

        try{
          write_read_on_serialline("INTERN!",false);
          ministat_data_.has_write_error = false;
        }
        catch (Tango::DevFailed& df)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::IS_INT_PROBE_USED_MSG";
          ministat_data_.error_desc = "failed to to IS_INT_PROBE_USED_MSG, yat exception caught<"
                         +  std::string (df.errors[0].desc) + "\n";
          return;
        }
        catch (...)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::IS_INT_PROBE_USED_MSG";
          ministat_data_.error_desc = "failed to IS_INT_PROBE_USED_MSG, (...) caught\n";
          return;
        }
      }
      break;

      //- STOP_REGULATION ------------------- 
    case STOP_REGULATION_MSG:
      {
        DEBUG_STREAM<<" STOP_REGULATION_MSG entering ...  "<<endl;
        yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);

        try{
          write_read_on_serialline("KM_OFF",false);
          ministat_data_.has_write_error = false;
        }
        catch (Tango::DevFailed& df)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::STOP_REGULATION_MSG";
          ministat_data_.error_desc = "failed to to STOP_REGULATION_MSG, yat exception caught<"
                         +  std::string (df.errors[0].desc) + "\n";
          return;
        }
        catch (...)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::STOP_REGULATION_MSG";
          ministat_data_.error_desc = "failed to STOP_REGULATION_MSG, (...) caught\n";
          return;
        }
      }
      break;

      //- START_REGULATION ------------------- 
    case START_REGULATION_MSG:
      {
        DEBUG_STREAM<<" START_REGULATION_MSG entering ...  "<<endl;
        yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);

        try{
          write_read_on_serialline("KM_ON",false);
          ministat_data_.has_write_error = false;
        }
        catch (Tango::DevFailed& df)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::START_REGULATION_MSG";
          ministat_data_.error_desc = "failed to to START_REGULATION_MSG, yat exception caught<"
                         +  std::string (df.errors[0].desc) + "\n";
          return;
        }
        catch (...)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::START_REGULATION_MSG";
          ministat_data_.error_desc = "failed to START_REGULATION_MSG, (...) caught\n";
          return;
        }
      }
      break;

      // - WRITE_LOW_TEMP_ALARM_MSG ------------------- 
    case WRITE_LOW_TEMP_ALARM_MSG:
      {
        double * low_temp_alarm;
        yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);

        try{
          _msg.detach_data (low_temp_alarm);
        }
        catch (yat::Exception& ex)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::WRITE_LOW_TEMP_ALARM_MSG";
          ministat_data_.error_desc = "failed to WRITE_LOW_TEMP_ALARM_MSG, DevFailed caught<"
                         +  std::string (ex.errors[0].desc) + "\n";
          return;
        }
        write_low_alarm_temperature (*low_temp_alarm);
      }
      break;

    case WRITE_HIGH_TEMP_ALARM_MSG:
      {
        double * high_temp_alarm;
        yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);

        try{
          _msg.detach_data (high_temp_alarm);
        }
        catch (yat::Exception& ex)
        {
          ministat_data_.has_write_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::handle_message::WRITE_HIGH_TEMP_ALARM_MSG";
          ministat_data_.error_desc = "failed toWRITE_HIGH_TEMP_ALARM_MSG, yat exception caught<"
                         +  std::string (ex.errors[0].desc) + "\n";
          return;
        }
        write_high_alarm_temperature(*high_temp_alarm);
      }
      break;
    default:
      DEBUG_STREAM << "MinistatCommunicationTask::handle_message::unhandled msg type received"<< std::endl;

    }// end switch
  }

  // **************************************************************************
  //						write_read_on_serialline
  //
  //    send the specified command to the Ministat125 and return its response
  //	argin1 : string command
  //    argin2: read_required is set to true if Ministat125 answers to the command and method should wait for an answer
  //		argout : string response
  //
  // **************************************************************************
  std::string MinistatCommunicationTask::write_read_on_serialline(std::string cmd_to_send,bool read_required)
  { 
    DEBUG_STREAM << "write_read_on_serialline(): entering... cmd_to_send =" << cmd_to_send << endl;

    if(this->serial_proxy_ == 0) 
    {
      WARN_STREAM<<"no Serial device proxy selected"<<endl;
      return "";
    }

    //- ************************CRITICAL SECTION**************************
    yat::AutoMutex <yat::Mutex> guard (this->com_mutex);
    {
      long nb_char_written;    
      std::string response;
      Tango::DevVarCharArray * CR_LF_=0;
      // Construct a dedicated DevVarCharArray with validation char <CR_LF>

      CR_LF_ = new Tango::DevVarCharArray();
      CR_LF_->length(2);

      (*CR_LF_)[0] = 13;
      (*CR_LF_)[1] = 10;

      DEBUG_STREAM<<" cmd to send "<<cmd_to_send<<endl;

      // each command sending is consisted of 3 steps:
      // -1- write ministat command in the buffer
      // -2- write CR & LF Characters in the buffer
      // -3- read buffer if reading expected
      Tango::DevLong mode = 2;

      _DEV_TRY(
        this->serial_proxy_->command_in("DevSerFlush",mode),
        "send command to the hardware",
        "Ministat125::write_read_on_serialline");

      //- write command
      _DEV_TRY(
        this->serial_proxy_->command_inout("DevSerWriteString", cmd_to_send.c_str(),nb_char_written),
        "send command to the hardware",
        "Ministat125::write_read_on_serialline");

      //- write CR_LF characters
      _DEV_TRY(
        this->serial_proxy_->command_in("DevSerWriteChar",CR_LF_),
        "send CR & LF characters to the hardware",
        "Ministat125::write_read_on_serialline");

      DEBUG_STREAM<<cmd_to_send<<" command sent!!"<<endl;

      //- read Ministat125 response if required 
      if (read_required==true)
      {
        _DEV_TRY(
          this->serial_proxy_->command_out("DevSerReadLine", response),
          "read hardware response",
          "Ministat125::write_read_on_serialline");

        DEBUG_STREAM<<"response received: "<< response <<endl;

        return response;
      }
      else
      {
        return "no expected response";
      }
    } //- *******************END CRITICAL SECTION**************************

  }//end function

  ////////////////////////////////////////////////////////////////////////////////
  //
  //      read_external_temperature()
  //
  ////////////////////////////////////////////////////////////////////////////////
  void MinistatCommunicationTask::read_external_temperature()
  {
    std::string response;

    try{
      ministat_data_.temperature_ext = yat::IEEE_NAN;
      response = write_read_on_serialline("EXTERN?",true);
      response = response.substr(0,response.length()-1);
      ministat_data_.temperature_ext = XString<double>::convertFromString(response);
      ministat_data_.has_error = false;
    }
    catch (Tango::DevFailed& df)
    {
          ministat_data_.has_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::read_external_temperature";
          ministat_data_.error_desc = "failed to read_external_temperature, DevFailed caught<"
                         +  std::string (df.errors[0].desc) + "\n";
    }
    catch (...)
    {
          ministat_data_.has_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::read_external_temperature";
          ministat_data_.error_desc = "failed to read_external_temperature, (...) caught\n";
    }      
  }

  ////////////////////////////////////////////////////////////////////////////////
  //
  //      read_preset_temperature()
  //
  ////////////////////////////////////////////////////////////////////////////////
  void MinistatCommunicationTask::read_preset_temperature()
  {
    std::string response;
    try{
      ministat_data_.temperature_preset = yat::IEEE_NAN;
      response = write_read_on_serialline("SETPOINT?",true);
    // exemple of the response pattern : <+ 12.3>, to get the value we have to evaluate the sign 
    // afterward, cutting the string till the 2nd element of the string (here "1")
    // get the sign:
      std::string sign = response.substr(0,1);
      DEBUG_STREAM <<"temperature_preset = "<<response.substr(2,response.length() - 2)<<endl;
      ministat_data_.temperature_preset =  XString<double>::convertFromString(response.substr(2,response.length() - 2));        

      if(sign == "-")
      {
        ministat_data_.temperature_preset *= -1;
      }  
      ministat_data_.has_error = false;
    }
    catch (Tango::DevFailed& df)
    {
          ministat_data_.has_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::read_preset_temperature";
          ministat_data_.error_desc = "failed to read_preset_temperature, DevFailed caught<"
                         +  std::string (df.errors[0].desc) + "\n";
    }
    catch (...)
    {
          ministat_data_.has_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::read_preset_temperature";
          ministat_data_.error_desc = "failed to read_preset_temperature, (...) caught\n";
    }      
  }

  ////////////////////////////////////////////////////////////////////////////////
  //
  //      read_internal_temperature()
  //
  ////////////////////////////////////////////////////////////////////////////////
  void MinistatCommunicationTask::read_internal_temperature()
  {
    std::string response;
    try{
      ministat_data_.temperature_int = yat::IEEE_NAN;
      response = write_read_on_serialline("INTERN?",true);
      response = response.substr(0,response.length()-1);

      DEBUG_STREAM <<"temperature_int = "<<response<<endl;
      ministat_data_.temperature_int = XString<double>::convertFromString(response);
      ministat_data_.has_error = false;
    }

    catch (Tango::DevFailed& df)
    {
          ministat_data_.has_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::read_internal_temperature";
          ministat_data_.error_desc = "failed to read_internal_temperature, DevFailed caught<"
                         +  std::string (df.errors[0].desc) + "\n";
    }
    catch (...)
    {
          ministat_data_.has_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::read_internal_temperature";
          ministat_data_.error_desc = "failed to read_internal_temperature, (...) caught\n";
    }  
  }

  ////////////////////////////////////////////////////////////////////////////////
  //
  //      read_probe_used()
  //
  ////////////////////////////////////////////////////////////////////////////////
  void MinistatCommunicationTask::read_probe_used()
  {
    std::string response;
    try{
      response = write_read_on_serialline("TEMP?",true);
      response = response.substr(0,response.length()-2);  // remove CR & LF characters
      if(response == "INTERN")
        ministat_data_.externProbeUsed = false; 
      else if(response == "EXTERN")
        ministat_data_.externProbeUsed = true;
      else
        throw;
      ministat_data_.has_error = false;
    }

    catch (Tango::DevFailed& df)
    {
          ministat_data_.has_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::read_probe_used";
          ministat_data_.error_desc = "failed to read_probe_used, DevFailed caught<"
                         +  std::string (df.errors[0].desc) + "\n";
    }
    catch (...)
    {
          ministat_data_.has_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::read_probe_used";
          ministat_data_.error_desc = "failed to read_probe_used, (...) caught\n";
    }
    

  }

  ////////////////////////////////////////////////////////////////////////////////
  //
  //      read_error()
  //
  ////////////////////////////////////////////////////////////////////////////////
  void MinistatCommunicationTask::read_error()
  {
    std::string response;
    try{
      response = write_read_on_serialline("error?",true);
      response = response.substr(0,response.length()-2);  // remove CR & LF characters
      if (response != "ERROR 0")
        ministat_data_.error_occured =  true;
      else
        ministat_data_.error_occured =  false;
      ministat_data_.has_error = false;
    }
    catch (Tango::DevFailed& df)
    {
          ministat_data_.has_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::read_error";
          ministat_data_.error_desc = "failed to read_error, DevFailed caught<"
                         +  std::string (df.errors[0].desc) + "\n";
    }
    catch (...)
    {
          ministat_data_.has_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::read_error";
          ministat_data_.error_desc = "failed to read_error, (...) caught\n";
    }
  }

  ////////////////////////////////////////////////////////////////////////////////
  //
  //      read_regulation_mode()
  //
  ////////////////////////////////////////////////////////////////////////////////
  void MinistatCommunicationTask::read_regulation_mode()
  {
    std::string response, regulation;

    try{
      response = write_read_on_serialline("KM?",true);
    regulation = response.substr(0,response.length()-2);  // delete CR_LF characters
    if(regulation == "OFF")
      ministat_data_.regulation_enabled = false;
    else if(regulation=="ON")
      ministat_data_.regulation_enabled = true;
    else //- goto traitement d'erreur
      throw;
    ministat_data_.has_error = false;
    }
    catch (Tango::DevFailed& df)
    {
          ministat_data_.has_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::read_regulation_mode";
          ministat_data_.error_desc = "failed to read_regulation_mode, DevFailed caught<"
                         +  std::string (df.errors[0].desc) + "\n";
    }
    catch (...)
    {
          ministat_data_.has_error = true;
          ministat_data_.error_origin = "MinistatCommunicationTask::read_regulation_mode";
          ministat_data_.error_desc = "failed to read_regulation_mode, (...) caught\n";
    }
  }

  ////////////////////////////////////////////////////////////////////////////////
  //
  //      write_low_alarm_temperature()
  //
  ////////////////////////////////////////////////////////////////////////////////
  void MinistatCommunicationTask::write_low_alarm_temperature(double low_alarm_temp)
  {
    DEBUG_STREAM<<"MinistatCommunicationTask::write_low_alarm_temperature entering ... "<<endl;

    std::string cmd_limit,low_limit;
    try
    {
      if(low_alarm_temp >= LIMIT_MIN)
      {
        int tmp = static_cast <int> (low_alarm_temp);
        low_limit = XString<int>::convertToString(tmp);
          cmd_limit = "LO_LIMIT " + low_limit;
          write_read_on_serialline(cmd_limit,false);
          ministat_data_.has_write_error = false;
      }
      else
      {
        ministat_data_.has_write_error = true;
        ministat_data_.error_origin = "MinistatCommunicationTask::write_low_alarm_temperature";
        ministat_data_.error_desc = "failed to write_low_alarm_temperature, out of range\n";
      }
    }
    catch (...)
    {
      ministat_data_.has_write_error = true;
      ministat_data_.error_origin = "MinistatCommunicationTask::write_low_alarm_temperature";
      ministat_data_.error_desc = "failed to write_low_alarm_temperature, (...) caught\n";
    }
  }
  ////////////////////////////////////////////////////////////////////////////////
  //
  //      write_high_alarm_temperature()
  //
  ////////////////////////////////////////////////////////////////////////////////
  void MinistatCommunicationTask::write_high_alarm_temperature(double high_alarm_temp)
  {
    DEBUG_STREAM<<"MinistatCommunicationTask::write_high_alarm_temperature entering ... "<<endl;

    std::string cmd_limit,high_limit;
    try
    {
      if(high_alarm_temp < LIMIT_MAX)
      {
        int tmp = static_cast <int> (high_alarm_temp);
        high_limit = XString<int>::convertToString(tmp);
          cmd_limit = "HI_LIMIT " + high_limit;
          write_read_on_serialline(cmd_limit,false);
          ministat_data_.has_write_error = false;
      }
      else
      {
        ministat_data_.has_write_error = true;
        ministat_data_.error_origin = "MinistatCommunicationTask::write_high_alarm_temperature";
        ministat_data_.error_desc = "failed to write_high_alarm_temperature, out of range\n";
      }
    }
    catch (...)
    {
      ministat_data_.has_write_error = true;
      ministat_data_.error_origin = "MinistatCommunicationTask::write_high_alarm_temperature";
      ministat_data_.error_desc = "failed to write_high_alarm_temperature, (...) caught\n";
    }
  }

  ////////////////////////////////////////////////////////////////////////////////
  //
  //      write_preset_temperature()
  //
  ////////////////////////////////////////////////////////////////////////////////
  void MinistatCommunicationTask::write_preset_temperature(double t_preset)
  {
    std::stringstream s;
    s << "SET " << std::fixed << std::setprecision (1) << t_preset;
    try
    {
      write_read_on_serialline (s.str (), false);
      ministat_data_.has_write_error = false;
    }
    catch (...)
    {
      ministat_data_.has_write_error = true;
      ministat_data_.error_origin = "MinistatCommunicationTask::write_preset_temperature";
      ministat_data_.error_desc = "failed to write_preset_temperature, (...) caught\n";
    }
  }  
  ////////////////////////////////////////////////////////////////////////////////
  //
  //      get_proxy()
  //
  ////////////////////////////////////////////////////////////////////////////////
  Tango::DeviceProxyHelper * MinistatCommunicationTask::get_proxy(void)
  {
    return this->serial_proxy_;
  }

  ////////////////////////////////////////////////////////////////////////////////
  //
  //      get_data()
  //
  ////////////////////////////////////////////////////////////////////////////////
  MinistatData * MinistatCommunicationTask::get_data()
    throw (Tango::DevFailed)
  {
    //- enter critical section
    // we use an auto mutex -> mutex unlock() whatever the situation (even if an exception is sent in the critical section)

    MinistatData * data = 0;
    yat::AutoMutex <yat::Mutex>  guard(this->data_mutex_);
    {
      data = new MinistatData;

      if(data==0)
        THROW_DEVFAILED (_CPTC("MEMORY_ALLOC_FAILED"),
                          _CPTC("Failed to alocate data structure call software support]"),
                          _CPTC("MinistatCommunicationTask::get_data()"));

      *data = ministat_data_;
    }
    return data;
  }

}  // namespace
