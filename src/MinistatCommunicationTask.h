// MinistatCommunicationTask.h: interface for the MinistatCommunicationTask class.
//
//////////////////////////////////////////////////////////////////////////////////

#ifndef _MINISTAT_COMMUNICATION_TASK_H_
#define _MINISTAT_COMMUNICATION_TASK_H_

#include <tango.h>
#include <DeviceProxyHelper.h>
#include <Xstring.h>
#include <yat/threading/Task.h>
#include <yat/threading/Mutex.h>
#include <yat4tango/CommonHeader.h>
#include <yat4tango/ExceptionHelper.h>

namespace Ministat125_ns
{
// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================

// READ MESSAGE ARE PERIODIC

// WRITE MESSAGE ARE ASYNCHRONOUS
const size_t WRITE_PRESET_TEMP_MSG = yat::FIRST_USER_MSG + 1;
const size_t IS_REMOTE_MSG = yat::FIRST_USER_MSG + 2;
const size_t IS_EXT_PROBE_USED_MSG = yat::FIRST_USER_MSG + 3;
const size_t STOP_REGULATION_MSG = yat::FIRST_USER_MSG + 4;
const size_t START_REGULATION_MSG = yat::FIRST_USER_MSG + 5;
const size_t IS_LOCAL_MSG = yat::FIRST_USER_MSG + 6;
const size_t IS_INT_PROBE_USED_MSG = yat::FIRST_USER_MSG + 7;
const size_t WRITE_LOW_TEMP_ALARM_MSG = yat::FIRST_USER_MSG + 8;
const size_t WRITE_HIGH_TEMP_ALARM_MSG = yat::FIRST_USER_MSG + 9;


// time sleep constant 
const size_t TIME_SLEEP= 1000;

// -25�C < range temperature < 150�C
const double LIMIT_MAX = 150;    
const double LIMIT_MIN = -25;

const size_t NUMBER_OF_READ_COMMAND_USED = 4;
    typedef enum
    {
        EXTERNAL_TEMP_REQUEST,
        PRESET_TEMP_REQUEST,
        INTERNAL_TEMP_REQUEST,
        PROBE_USED_REQUEST,
        ERROR_REQUEST,
        REGULATION_MODE_REQUEST
    }request_type;

    typedef struct Data
    {
        double temperature_ext;
        double temperature_int;
        double temperature_preset;
        bool externProbeUsed;
        bool error_occured;
        bool regulation_enabled;
        bool remote_mode;
        bool has_error;
        bool has_write_error;
        std::string error_origin;
        std::string error_desc;
    }MinistatData;

    class MinistatCommunicationTask : public yat::Task, public Tango::LogAdapter
    {
    public:
        /*
        * CTOR :
        */
        MinistatCommunicationTask(Tango::DeviceImpl* _host_device,
            Tango::DeviceProxyHelper * dev_proxy);
        
        /*
        * DTOR :
        */
        virtual ~MinistatCommunicationTask();
   
        /*
        * get_data
        */        
        MinistatData * get_data()    throw (Tango::DevFailed);

        Tango::DeviceProxyHelper * get_proxy(void);

    protected:
        
        //- handle_message -----------------------
        //- execute the process corresponding to the older message of the message queue
        // - use a counter to select the reading action (ex: temp_ext --3s--> temp_in --3s--> temp_preset ...)
        virtual void handle_message (yat::Message& msg)
            throw (yat::Exception);
        
        std::string write_read_on_serialline(std::string cmd_to_send,bool read_required);
        void read_external_temperature(void);
        void read_preset_temperature(void);
        void read_internal_temperature(void);
        void read_probe_used(void);
        void read_error(void);
        void read_regulation_mode(void);


        //write methodes
        void write_preset_temperature(double t_preset);
	    void write_high_alarm_temperature(double high_alarm_temp);
	    void write_low_alarm_temperature(double low_alarm_temp);

    private:

      yat::Mutex com_mutex;
        
        MinistatData ministat_data_;
        yat::Mutex data_mutex_;
        Tango::DeviceProxyHelper * serial_proxy_;
        size_t command_counter_;


        
    };
    
}   //  namespace
#endif
