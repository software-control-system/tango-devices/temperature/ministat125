static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/Instrumentation/Ministat125/src/ClassFactory.cpp,v 1.3 2007-03-20 11:43:30 stephle Exp $";
//+=============================================================================
//
// file :        ClassFactory.cpp
//
// description : C++ source for the class_factory method of the DServer
//               device class. This method is responsible to create
//               all class singletin for a device server. It is called
//               at device server startup
//
// project :     TANGO Device Server
//
// $Author: stephle $
//
// $Revision: 1.3 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>
#include <Ministat125Class.h>

/**
 *	Create Ministat125Class singleton and store it in DServer object.
 */

void Tango::DServer::class_factory()
{

	add_class(Ministat125_ns::Ministat125Class::init("Ministat125"));

}
