#!/usr/bin/env python
#
# ------------------------------------------------------------------------------
#
# file : callSerialLineForMinistat.py
#
# description :
#
#
# project : Swing 
#
#
# $Author: sebleport $
#
# $Revision: 1.2 $
# ------------------------------------------------------------------------------
import sys
import string
import time
from PyTango import *

def send_message_to_serial(dev, message, read_required):

    try:
        # Vider le buffer E/S
        dev.command_inout("DevSerFlush", 2)
        #print "DevSerFlush"

        # Envoyer la commande
        nb_char_written = dev.command_inout("DevSerWriteString",message)
        #print "DevSerWriteString return value = " + str(nb_char_written)

        # Envoyer le carat�re de fin de string
        charEndMessage = []
        charEndMessage.append(13)
        charEndMessage.append(10)        
        dev.command_inout("DevSerWriteChar",charEndMessage)
        # Tango::DevVarCharArray * CR_LF_=0; (*CR_LF_)[0] = 13;
        #(*CR_LF_)[1] = 10;

        if read_required:
            #print "before response"
            response = dev.command_inout("DevSerReadLine")
            #print "response received: " + response
            return response
    except Exception, ex:
        print " ======> An Exception has been received in send_message_to_serial during : " + message, ex

    return "NO_REPONSE_FOR " + message 
        
def main():
    "Main program"
    
    # Recuperation du device proxy
    try:
        dev = DeviceProxy("I11-C-C00/CA/RS232_8.1-COM4")

	waiting_time = 3
	req_num = 0

        #Recuperation de la temperature interne
        response = send_message_to_serial(dev,"INTERN?",1)
	req_num = req_num + 1
        print str(req_num) + " - Read Internal temperature : " + str(response)
	time.sleep(waiting_time)

        #Recuperation de la temperature externe
        response = send_message_to_serial(dev,"EXTERN?",1)
        req_num = req_num + 1
        print str(req_num) + " - Read External temperature : " + str(response)
	time.sleep(waiting_time)

        #Recuperation de la temperature de consigne
        response = send_message_to_serial(dev,"SETPOINT?",1)
        req_num = req_num + 1
        print str(req_num) + " - Read Setpoint temperature : " + str(response)
	time.sleep(waiting_time)

        #Recuperation du mode de thermo-regulation
        response = send_message_to_serial(dev,"TEMP?",1)
        req_num = req_num + 1
        print str(req_num) + " - Read Regulation Mode : " + str(response)
	time.sleep(waiting_time)

        #Ecriture de la consigne
        response = send_message_to_serial(dev,"SET 30.0",0)
        req_num = req_num + 1
        print str(req_num) + " - Setpoint to 30 "
	time.sleep(waiting_time)

        #Recuperation de la temperature de consigne
        response = send_message_to_serial(dev,"SETPOINT?",1)
        req_num = req_num + 1
        print str(req_num) + " - Read Setpoint temperature : " + str(response)
	time.sleep(waiting_time)

################################################################################
# ATTENTION - A UTILISER UNIQUEMENT EN MODE NON REGULE
################################################################################
#        #Lecture de l'�tat de r�gulation
#        response = send_message_to_serial(dev,"KM?",1)
#        req_num = req_num + 1
#        print str(req_num) + " - Read Regulation State : " + str(response)
#	time.sleep(waiting_time)
################################################################################

################################################################################
# PASSAGE en mode Regulation
################################################################################

        #Passage en mode r�gulation
        send_message_to_serial(dev,"KM_ON",0)
        req_num = req_num + 1
        print str(req_num) + " - ************* Request Regulation Received **************"
	time.sleep(waiting_time)

        #Recuperation de la temperature interne
        response = send_message_to_serial(dev,"INTERN?",1)
	req_num = req_num + 1
        print str(req_num) + " - Read Internal temperature : " + str(response)
	time.sleep(waiting_time)

        #Recuperation de la temperature externe
        response = send_message_to_serial(dev,"EXTERN?",1)
        req_num = req_num + 1
        print str(req_num) + " - Read External temperature : " + str(response)
	time.sleep(waiting_time)

        #Recuperation de la temperature de consigne
        response = send_message_to_serial(dev,"SETPOINT?",1)
        req_num = req_num + 1
        print str(req_num) + " - Read Setpoint temperature : " + str(response)
	time.sleep(waiting_time)

        #Recuperation du mode de thermo-regulation
        response = send_message_to_serial(dev,"TEMP?",1)
        req_num = req_num + 1
        print str(req_num) + " - Read Regulation Mode : " + str(response)
	time.sleep(waiting_time)

        #Ecriture de la consigne
        response = send_message_to_serial(dev,"SET 40.0",0)
        req_num = req_num + 1
        print str(req_num) + " - Setpoint to 40 "
	time.sleep(waiting_time)

        #Recuperation de la temperature de consigne
        response = send_message_to_serial(dev,"SETPOINT?",1)
        req_num = req_num + 1
        print str(req_num) + " - Read Setpoint temperature : " + str(response)
	time.sleep(waiting_time)

	# Lecture de la r�gulation n fois de suite
	for i in range(10):
        	#Recuperation de la temperature interne
        	response = send_message_to_serial(dev,"INTERN?",1)
		req_num = req_num + 1
        	print str(req_num) + " - Read Internal temperature : " + str(response)
		time.sleep(waiting_time)

        #Passage en mode r�gulation off
        send_message_to_serial(dev,"KM_OFF",0)
        req_num = req_num + 1
        print str(req_num) + " - *************  Request Stop Regulation *************"
	time.sleep(waiting_time)

	# Lecture de la r�gulation n fois de suite
	for i in range(10):
        	#Recuperation de la temperature interne
        	response = send_message_to_serial(dev,"INTERN?",1)
		req_num = req_num + 1
        	print str(req_num) + " - Read Internal temperature : " + str(response)
		time.sleep(waiting_time)
		
    except Exception, ex:
        print " ======> An Exception has been received : ", ex
       
if __name__ == '__main__':
    main()
    
